
package semaforo;

import java.applet.Applet;
import java.awt.Color;
import javax.swing.JTextArea;
import javax.swing.JTextField;


public class Semaforo extends Applet implements Runnable {

    Thread hilo1;
    
    JTextField verde1, naranja1, rojo1,
               verde2, naranja2, rojo2,
               verde3, naranja3, rojo3,
               verde4, naranja4, rojo4,
               verde5, naranja5, rojo5,
               verde6, naranja6, rojo6,
               verde7, naranja7, rojo7,
               verde8, naranja8, rojo8;
               
    JTextArea semaforo1, semaforo2,semaforo3,semaforo4,semaforo5,semaforo6,semaforo7,semaforo8,
              Calle1,Calle2,Calle3,Calle4,Calle5,Calle6,Calle7,Calle8,Calle9,
              Carrito1,Carrito2,Carrito3,Carrito4;
    
    String wacho;
    
    int tiempo1, tiempo2;
    
   public void init ()
   {
       setLayout(null);
       
       //cada color de semaforo por defecto estan todos en blancos .............
       
       verde1 = new JTextField();
       verde1.setBackground(Color.white);
       verde1.setBounds(300, 40, 30, 30);//300
       add(verde1);
       
       naranja1 = new JTextField();
       naranja1.setBackground(Color.white);
       naranja1.setBounds(300, 70, 30, 30);
       add(naranja1);
       
       rojo1 = new JTextField();
       rojo1.setBackground(Color.white);
       rojo1.setBounds(300, 100, 30, 30);
       add(rojo1);
       
       verde2 = new JTextField();
       verde2.setBackground(Color.white);
       verde2.setBounds(530, 280, 30, 30);//300
       add(verde2);
       
       naranja2 = new JTextField();
       naranja2.setBackground(Color.white);
       naranja2.setBounds(560, 280, 30, 30);
       add(naranja2);
       
       rojo2 = new JTextField();
       rojo2.setBackground(Color.white);
       rojo2.setBounds(590, 280, 30, 30);
       add(rojo2);
       
       verde3 = new JTextField();
       verde3.setBackground(Color.white);
       verde3.setBounds(530, 540, 30, 30);//300
       add(verde3);
       
       naranja3 = new JTextField();
       naranja3.setBackground(Color.white);
       naranja3.setBounds(560, 540, 30, 30);
       add(naranja3);
       
       rojo3 = new JTextField();
       rojo3.setBackground(Color.white);
       rojo3.setBounds(590, 540, 30, 30);
       add(rojo3);
       
       verde4 = new JTextField();
       verde4.setBackground(Color.white);
       verde4.setBounds(1040, 540, 30, 30);//300
       add(verde4);
       
       naranja4 = new JTextField();
       naranja4.setBackground(Color.white);
       naranja4.setBounds(1070, 540, 30, 30);
       add(naranja4);
       
       rojo4 = new JTextField();
       rojo4.setBackground(Color.white);
       rojo4.setBounds(1100, 540, 30, 30);
       add(rojo4);
       
       verde5 = new JTextField();
       verde5.setBackground(Color.white);
       verde5.setBounds(1040, 280, 30, 30);//300
       add(verde5);
       
       naranja5 = new JTextField();
       naranja5.setBackground(Color.white);
       naranja5.setBounds(1070, 280, 30, 30);
       add(naranja5);
       
       rojo5 = new JTextField();
       rojo5.setBackground(Color.white);
       rojo5.setBounds(1100, 280, 30, 30);
       add(rojo5);
       
       verde6 = new JTextField();
       verde6.setBackground(Color.white);
       verde6.setBounds(810, 40, 30, 30);//300
       add(verde6);
       
       naranja6 = new JTextField();
       naranja6.setBackground(Color.white);
       naranja6.setBounds(810, 70, 30, 30);
       add(naranja6);
       
       rojo6 = new JTextField();
       rojo6.setBackground(Color.white);
       rojo6.setBounds(810, 100, 30, 30);
       add(rojo6);
       
       verde7 = new JTextField();
       verde7.setBackground(Color.white);
       verde7.setBounds(810, 300, 30, 30);//300
       add(verde7);
       
       naranja7 = new JTextField();
       naranja7.setBackground(Color.white);
       naranja7.setBounds(810, 330, 30, 30);
       add(naranja7);
       
       rojo7 = new JTextField();
       rojo7.setBackground(Color.white);
       rojo7.setBounds(810, 360, 30, 30);
       add(rojo7);
       
       verde8 = new JTextField();
       verde8.setBackground(Color.white);
       verde8.setBounds(300, 300, 30, 30);//300
       add(verde8);
       
       naranja8 = new JTextField();
       naranja8.setBackground(Color.white);
       naranja8.setBounds(300, 330, 30, 30);
       add(naranja8);
       
       rojo8 = new JTextField();
       rojo8.setBackground(Color.white);
       rojo8.setBounds(300, 360, 30, 30);
       add(rojo8);
       
       //carritos..............................
       
       Carrito1 = new JTextArea ();
       Carrito1.setBackground(Color.blue);
       Carrito1.setBounds(0, 160, 80, 30);
       add(Carrito1);
       
       Carrito2 = new JTextArea ();
       Carrito2.setBackground(Color.pink);
       Carrito2.setBounds(870, 280, 30, 80);
       add(Carrito2);
       
       Carrito3 = new JTextArea ();
       Carrito3.setBackground(Color.yellow);
       Carrito3.setBounds(510, 220, 80, 30);
       add(Carrito3);
       
       Carrito4 = new JTextArea ();
       Carrito4.setBackground(Color.magenta);
       Carrito4.setBounds(520, 480, 80, 30);
       add(Carrito4);
       
       //semaforos..........................
       
       semaforo1 = new JTextArea ();
       semaforo1.setBackground(Color.black);
       semaforo1.setBounds(280, 20, 70, 130);//70
       add(semaforo1);
       
       semaforo2 = new JTextArea ();
       semaforo2.setBackground(Color.black);
       semaforo2.setBounds(510, 260, 130, 70);
       add(semaforo2);
       
       semaforo3 = new JTextArea ();
       semaforo3.setBackground(Color.black);
       semaforo3.setBounds(510, 520, 130, 70);//510
       add(semaforo3);
       
       semaforo4 = new JTextArea ();
       semaforo4.setBackground(Color.black);
       semaforo4.setBounds(1020, 520, 130, 70);
       add(semaforo4);
       
       semaforo5 = new JTextArea ();
       semaforo5.setBackground(Color.black);
       semaforo5.setBounds(1020, 260, 130, 70);
       add(semaforo5);
       
       semaforo6 = new JTextArea ();
       semaforo6.setBackground(Color.black);
       semaforo6.setBounds(790, 20, 70, 130);
       add(semaforo6);
       
       semaforo7 = new JTextArea ();
       semaforo7.setBackground(Color.black);
       semaforo7.setBounds(790, 280, 70, 130);
       add(semaforo7);
       
       semaforo8 = new JTextArea ();
       semaforo8.setBackground(Color.black);
       semaforo8.setBounds(280, 280, 70, 130);
       add(semaforo8);
       
       //calles ................................
       
       Calle1 = new JTextArea ();
       Calle1.setBackground(Color.lightGray);
       Calle1.setBounds(0, 0, 350, 150);//150
       add(Calle1);
       
       Calle2 = new JTextArea ();
       Calle2.setBackground(Color.lightGray);
       Calle2.setBounds(0, 520, 350, 150);
       add(Calle2);
       
       Calle3 = new JTextArea ();
       Calle3.setBackground(Color.lightGray);
       Calle3.setBounds(1020, 0, 350, 150);//1020 0 350 150 
       add(Calle3);
       
       Calle4 = new JTextArea ();
       Calle4.setBackground(Color.lightGray);
       Calle4.setBounds(1020, 520, 350, 150);//150
       add(Calle4);
       
       Calle5 = new JTextArea ();
       Calle5.setBackground(Color.lightGray);
       Calle5.setBounds(510, 0, 350, 150);
       add(Calle5);
       
       Calle6 = new JTextArea ();
       Calle6.setBackground(Color.lightGray);
       Calle6.setBounds(510, 520, 350, 150);//1020 0 350 150 
       add(Calle6);
       
       Calle7 = new JTextArea ();
       Calle7.setBackground(Color.lightGray);
       Calle7.setBounds(0, 260, 350, 150);//150
       add(Calle7);
       
       Calle8 = new JTextArea ();
       Calle8.setBackground(Color.lightGray);
       Calle8.setBounds(510, 260, 350, 150);
       add(Calle8);
       
       Calle9 = new JTextArea ();
       Calle9.setBackground(Color.lightGray);
       Calle9.setBounds(1020, 260, 350, 150);//1020 0 350 150 
       add(Calle9);
       
       hilo1 = new Thread (this);
       hilo1.start();
       
   }
    @Override
    public void run() {
        
        Thread wacho = Thread.currentThread();
        while (wacho == hilo1)
        {
            
        }
    }
    
     public static void delaytiempo () {
        try
        {
            Thread.sleep(1000);
            
        } catch(InterruptedException e)
        {
            
        }
    }
    
}
